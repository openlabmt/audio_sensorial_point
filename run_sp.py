#!/usr/bin/python3

########################################################
# Program that controls the audio sensorial point      #
# It uses:                                             #
# - A HC-SR04 ultrasonic sensor for presence detection #
# TODO: update this below                              #
# - 3 push buttons to start the audio descriptions     #
# - 2 push buttons to select the languages             #
########################################################

import subprocess
import time
import logging
import signal
import RPi.GPIO as GPIO
import sys
import configparser
from multiprocessing import Process, Queue

LED_OK_PIN = 37
LED_PLAY_PIN = 40
LED_PINS = [LED_OK_PIN, LED_PLAY_PIN]

TRIG_PIN = 31
ECHO_PIN = 33

BUTTON_LANG_IT_PIN = 7
BUTTON_LANG_EN_PIN = 11

BUTTON_AUDIO_CC_PIN = 13
BUTTON_AUDIO_SB_PIN = 15
BUTTON_AUDIO_SA_PIN = 35
BUTTON_AUDIO_PINS = [BUTTON_AUDIO_CC_PIN, BUTTON_AUDIO_SB_PIN, BUTTON_AUDIO_SA_PIN]

BUTTON_PINS = [BUTTON_LANG_IT_PIN, BUTTON_LANG_EN_PIN, BUTTON_AUDIO_CC_PIN, BUTTON_AUDIO_SB_PIN, BUTTON_AUDIO_SA_PIN]

AUDIO_FILES = {
    BUTTON_AUDIO_CC_PIN: ['../resources/casa_cava_it.mp3', '../resources/casa_cava_en.mp3'],
    BUTTON_AUDIO_SB_PIN: ['../resources/san_pietro_barisano_it.mp3', '../resources/san_pietro_barisano_en.mp3'],
    BUTTON_AUDIO_SA_PIN: ['../resources/sant_agostino_it.mp3', '../resources/sant_agostino_en.mp3'],
}

LANG_IT = 0
LANG_EN = 1
ANN_LANG_FILES = ['../resources/lang_it.mp3', '../resources/lang_en.mp3']

AUDIO_INTRO_FILE = '../resources/audio_intro.mp3'

DEFAUT_LOG_LEVEL = logging.INFO

# Activate ultrasonic sensor every X s
PRESENCE_DETECT_FREQ = 1

# How many sensor reading to detect a presence
PRESENCE_DETECT_NB = 2

# Presence detection (cm)
PRESENCE_DETECT_AT = 50

# When a button is pressed we get value 0
BUTTON_PRESSED = 0

BUTTON_PRESSED_DELAY = 0.2
BUTTON_AUDIO_RESTART_DELAY = 0.5

# Time minimal (in s) between two intro descriptions
DELAY_BETWEEN_AUDIO_INTRO = 5

# Keep trace of media playing
media = {
    'audio_intro': {
        'process': None,
    },
    'ann_lang': {
        'process': None,
    },
    'audio_desc': {
        'process': None,
    },
}

def is_media_playing(media_type):
    ps = media[media_type]['process']
    if ps == None:
        return False
    return ps.poll() == None

def play_media(media_type, media_file):
    global media
    GPIO.output(LED_PLAY_PIN, GPIO.HIGH)
    logging.debug("Playing media: '%s'" % media_file)

    ps = subprocess.Popen(['omxplayer', media_file],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                close_fds=True
    ) 

    media[media_type]['process'] = ps

def stop_media(media_type):
    global media
    GPIO.output(LED_PLAY_PIN, GPIO.LOW)
    if is_media_playing(media_type):
        media[media_type]['process'].stdin.write(bytes('q', 'UTF-8')) # stdin.write('q') in Python2
        media[media_type]['process'] = None
        logging.debug("Media stopped")

def play_ann_lang(num_lang):
    logging.debug("Button language %s pressed" % num_lang)
    stop_media('audio_intro')
    stop_media('audio_desc')
    time.sleep(0.2)
    play_media('ann_lang', ANN_LANG_FILES[num_lang])
    while is_media_playing('ann_lang'):
        pass

def get_presence_dist():
    logging.debug("Starting ultrasonic sensor...")
    pulse_start = 0
    pulse_end = 0
    # Don't put ANY other instructions between here and the end of the 2nd while
    # otherwhise it will distort the echo measurement
    GPIO.output(TRIG_PIN, True)
    time.sleep(0.00001)
    GPIO.output(TRIG_PIN, False)
    while GPIO.input(ECHO_PIN) == 0: # wait until echo pin is high (1)
        pulse_start = time.time()

    while GPIO.input(ECHO_PIN) == 1: # wait until echo pin is low (0)
        pulse_end = time.time()

    pulse_duration = pulse_end - pulse_start

    # Calculate the distance (speed of the sound in the air: 343 m/s => 34300 cm/s)
    ## distance = speed * time
    ## distance = 34300 * time
    # As the echo gives the time it takes to travel to the object and back again
    # we need to divide it by two
    ## distance = 17150 * time
    distance = 17150 * pulse_duration
    distance = round(distance, 2)
    logging.debug("Ultrasonic distance: %s" % distance)
    
    return distance

def is_presence():
    GPIO.output(LED_PLAY_PIN, GPIO.LOW)
    distance = get_presence_dist()
    logging.debug("=== Checking #%s time(s) to validate presence" % PRESENCE_DETECT_NB)

    for i in range(0, PRESENCE_DETECT_NB):
        logging.debug("#%i" % i)
        
        distance = get_presence_dist()
        if distance <= 0:
            logging.error("Error reading distance")
            i = i -1 # Read it again!
        if distance > PRESENCE_DETECT_AT:
            return False
        time.sleep(1)

    return True

def gpio_setup():
    GPIO.setmode(GPIO.BOARD) # Use board pin numbering

    for button_pin in BUTTON_PINS:
        GPIO.setup(button_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    for led_pin in LED_PINS:
        GPIO.setup(led_pin, GPIO.OUT)

    GPIO.setup(TRIG_PIN, GPIO.OUT) # trigger => output
    GPIO.setup(ECHO_PIN, GPIO.IN)  # echo => input
    #GPIO.add_event_detect(my_config['echoPin'], GPIO.BOTH)

    GPIO.output(TRIG_PIN, False) # Ensure that the Trigger pin is set low
    
def signal_ctrl_c(signal, frame):
    logging.info("Ctrl+C pressed!")
    GPIO.cleanup()
    sys.exit(0)

def print_usage():
    print("Usage: %s [log_level] [path_to_log_file]" % sys.argv[0])

if __name__ == "__main__":
    log_file = None
    log_level = DEFAUT_LOG_LEVEL

    if len(sys.argv) > 1:
        level = sys.argv[1].upper()
        log_level = getattr(logging, level)
        if len(sys.argv) > 2:
            log_file = sys.argv[2]

    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=log_level, filename=log_file)

    signal.signal(signal.SIGINT, signal_ctrl_c)
    
    gpio_setup()

    logging.info("Ultrasonic sensor warmup...")
    time.sleep(2)
    for i in range(0, 5):
        get_presence_dist()
        time.sleep(1)
    logging.info("Done, starting the app!")

    presence_reading_delay = PRESENCE_DETECT_FREQ
    presence_check_time = 0
    btn_audio_pressed_time = 0
    presence_detected = False
    cur_lang = LANG_IT
    while True:
        GPIO.output(LED_OK_PIN, GPIO.HIGH)
        time.sleep(0.1)
        GPIO.output(LED_OK_PIN, GPIO.LOW)
        time.sleep(0.1)

        if GPIO.input(BUTTON_LANG_IT_PIN) == BUTTON_PRESSED:
            logging.debug("Button lang %s pressed" % BUTTON_LANG_IT_PIN)
            cur_lang = LANG_IT
            play_ann_lang(LANG_IT)

        if GPIO.input(BUTTON_LANG_EN_PIN) == BUTTON_PRESSED:
            logging.debug("Button lang %s pressed" % BUTTON_LANG_EN_PIN)
            cur_lang = LANG_EN
            play_ann_lang(LANG_EN)

        # Check if one of the audio desc button was pressed
        for button_pin in BUTTON_AUDIO_PINS:
            if GPIO.input(button_pin) == BUTTON_PRESSED:
                logging.debug("Button audio desc pressed")
                if not is_media_playing('audio_desc'):
                    stop_media('audio_intro')
                    logging.debug("Starting audio_desc")
                    play_media('audio_desc', AUDIO_FILES[button_pin][cur_lang])
                else:
                    if (time.time() - btn_audio_pressed_time) > BUTTON_AUDIO_RESTART_DELAY:
                        btn_audio_pressed_time = time.time()
                        logging.debug("Restarting audio_desc")
                        stop_media('audio_desc')
                        play_media('audio_desc', AUDIO_FILES[button_pin][cur_lang])
                time.sleep(BUTTON_PRESSED_DELAY)
                break
        
        if not is_media_playing('audio_desc')\
            and not is_media_playing('audio_intro')\
            and (time.time() - presence_check_time) > presence_reading_delay:
            
            presence_check_time = time.time()
            # Eventually reset reading delay (if it was changed after running an audio intro)
            presence_reading_delay = PRESENCE_DETECT_FREQ
            if is_presence():
                logging.debug("Presence detected")
                was_presence_detected = presence_detected
                presence_detected = True
                if was_presence_detected:
                    logging.debug("Somebody was already in front of the totem, do nothing")
                else:
                    logging.debug("Starting audio intro")
                    cur_lang = LANG_IT
                    play_media('audio_intro', AUDIO_INTRO_FILE)
                    presence_reading_delay = DELAY_BETWEEN_AUDIO_INTRO
                    logging.debug("End audio intro")
            else:
                logging.debug("No presence detected")
                presence_detected = False
